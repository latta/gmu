#!/usr/bin/python

import random

def rand():
	return str(random.uniform(-5.0,5.0))


#print "5.0 5.0 5.0"
#print "5.0 5.0 -5.0"
#print "5.0 -5.0 5.0"
#print "-5.0 5.0 5.0"
#print "-5.0 -5.0 5.0"
#print "-5.0 5.0 -5.0"
#print "5.0 -5.0 -5.0"
#print "-5.0 -5.0 -5.0"


# hrany ctverce o hrane delky 10, stred v 0,0,0
for i in range(0, 2000):
	print "5.0 "  + rand() + " " + rand()
	print "-5.0 " + rand() + " " + rand()
	print rand() + " 5.0 "  + rand()
	print rand() + " -5.0 " + rand()
	print rand() + " " + rand() + " 5.0"
	print rand() + " " + rand() + " -5.0"
