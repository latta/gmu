#version 430

//Tento shader vygeneruje kostky z bodu na spravne pozici

layout(points)in;
layout(triangle_strip,max_vertices=4)out;

out vec2 tc; // vystup texturovaci pozice

void main(){

    gl_Position= gl_in[0].gl_Position + vec4(-0.4, -0.4, 0, 0);
    tc = vec2(0,0);
    EmitVertex();

    gl_Position= gl_in[0].gl_Position + vec4(+0.4, -0.4, 0, 0);
    tc = vec2(1,0);
    EmitVertex();

    gl_Position= gl_in[0].gl_Position + vec4(-0.4, +0.4, 0, 0);
    tc = vec2(0,1);
    EmitVertex();

    gl_Position= gl_in[0].gl_Position + vec4(+0.4, +0.4, 0, 0);
    tc = vec2(1,1);
    EmitVertex();

    EndPrimitive();
}
