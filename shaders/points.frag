#version 430

in vec2 tc; // texturovaci koordinaty

layout(binding=0)uniform sampler2D tex;

out vec4 fragColor;


void main(){
        fragColor = texture(tex,tc);

        /*
        if (color.a < 0.2)
            fragColor = vec4(1,0,0,0.5);
        else
            fragColor = vec4(0,1,0,1);
            */
}
