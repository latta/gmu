#version 430

layout(location=0)in vec3 position;

out vec4 color;

/* Matice */
uniform mat4 v;
uniform mat4 p;
uniform mat4 m;
uniform bool cubeEnabled;


const vec4 colors[]=vec4[](
        vec4(1,0,0,1),
	vec4(0,1,0,1),
	vec4(0,0,1,1));

void main(){
    
    gl_Position=p*v*m*vec4(position,1);

    // vykreslovani krychle
    if (cubeEnabled)
        color = vec4(0,0,0,1);
    // vykreslovani os
    else
    {
        if (position.x != 0.0) // red
            color = colors[0];
        if (position.y != 0.0) // green
            color = colors[1];
        if (position.z != 0.0) // blue
            color = colors[2];
    }
}
