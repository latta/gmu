#include <limits>
#include <cmath>        // std::abs
#include <fstream>

#include "obb_cpu.h"

glm::vec3 vec_to_gmlvec3(vec v)
{
    glm::vec3 vektor(v.x,v.y,v.z);
    return vektor;
}

OBB_cpu::OBB_cpu(std::vector<vec> inPts, int pocet_generovanych_vrcholu=100)
{
    // z vygenerovanych bodu se udela convex hull

    const int n = pocet_generovanych_vrcholu;
    vec points[n];

    std::cout << n << std::endl;

    for (int i=0; i<n ; i++)
    {
        points[i] = inPts.at(i);
        //std::cout << i << " " << points[i] << std::endl;
    }

    convex_hull = Polyhedron::ConvexHull(points, n);

    for (int i=0; i<convex_hull.NumVertices(); i++)
        glm_convex_hull.push_back(vec_to_gmlvec3(convex_hull.Vertex(i)));
    return;
}

OBB_cpu::OBB_cpu(std::string filename)
{

    std::ifstream infile(filename);

    if(!infile)
    {
        std::cerr << "Soubor " << filename << " nelze otevrit" << std::endl;
        exit (EXIT_FAILURE);
    }

    std::vector <vec> points;
    float x, y, z;
    while (infile >> x >> y >> z)
    {
        points.push_back(vec(x,y,z));
    }

    convex_hull = Polyhedron::ConvexHull(&points[0], points.size());

    for (int i=0; i<convex_hull.NumVertices(); i++)
    {
        glm_convex_hull.push_back(vec_to_gmlvec3(convex_hull.Vertex(i)));
        //std::cout << convex_hull.Vertex(i)  << std::endl;
    }
    return;
}

OBB_result OBB_cpu::calculate_OBB_Bruteforce(int granularita=16)
{
    // tohle se bude vracet
    OBB_result minimal_OBB;

    // pocatecni nejvetsi nalezeny objem = nekonecno
    double minimal_volume = std::numeric_limits<double>::infinity();

    /*
    int num_hull_vertices = convex_hull.NumVertices();


    for (int j=0; j<num_hull_vertices; j++)
    {
        std::cout << "bod "<< j <<  "\tx=" << convex_hull.Vertex(j).x << "\t y=" << convex_hull.Vertex(j).y << "\t z=" << convex_hull.Vertex(j).z << std::endl;
    }
    */

    float DELTA_UHEL = PI/granularita;

    // postupne otacim vsechny body ruzne uhly natoceni a hledam, ktera pozice je nejvyhodnejsi
    for (int x=0; x<granularita; x++)
    {
        float UHEL_X = x*DELTA_UHEL;

        for (int y=0; y<granularita; y++)
        {
            float UHEL_Y = y*DELTA_UHEL;

            for (int z=0; z<granularita; z++)
            {

                float UHEL_Z = z*DELTA_UHEL;

                Quat q = Quat::FromEulerXYZ(UHEL_X, UHEL_Y, UHEL_Z);
                //Quat q(-1.0f + (x*2.0/granularita), -1.0f + (y*2.0/granularita), -1.0f + (z*2.0/granularita), 0.0f);

/*
                // struktura pro orotovane body
                vec rotated_points[num_hull_vertices];

                // rotujeme
                for (int i=0; i<num_hull_vertices; i++)
                {
                    rotated_points[i] = q.Transform(convex_hull.Vertex(i));
                }

                // z rotovanych bodu s stvori polyhedron, aby se daly pouzit funkce
                Polyhedron rotated_convex_hull = Polyhedron(rotated_points,num_hull_vertices);

*/
                Polyhedron rotated_convex_hull (convex_hull);
                rotated_convex_hull.Transform(q);

                // indexy krajnich bodu
                int x1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 1.0f,  0.0f,  0.0f));
                int x2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC(-1.0f,  0.0f,  0.0f));
                int y1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  1.0f,  0.0f));
                int y2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f, -1.0f,  0.0f));
                int z1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f,  1.0f));
                int z2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f, -1.0f));

                // souradnice krajnich bodu
                float x1 = rotated_convex_hull.Vertex(x1_index).x;
                float x2 = rotated_convex_hull.Vertex(x2_index).x;
                float y1 = rotated_convex_hull.Vertex(y1_index).y;
                float y2 = rotated_convex_hull.Vertex(y2_index).y;
                float z1 = rotated_convex_hull.Vertex(z1_index).z;
                float z2 = rotated_convex_hull.Vertex(z2_index).z;

                // objem
                float volume = (std::abs(x1) + std::abs(x2)) * (std::abs(y1) + std::abs(y2)) * (std::abs(z1) + std::abs(z2));

                if (volume < minimal_volume)
                {
                    //std::cout << "Nalezen novy nejmensi objem x=" << x << "\ty=" << y << "\tz=" << z << "\tV=" << volume << "\tUHEL_X=" << UHEL_X/PI*180 << "\tUHEL_Y=" << UHEL_Y/PI*180 << "\tUHEL_Z=" << UHEL_Z/PI*180 << std::endl;
                    //std::cout << "indexy: " << x1_index << " " << x2_index << " " << y1_index << " " << y2_index << " " << z1_index << " " << z2_index << " " << std::endl << std::endl;

                    const Quat qi = q.Inverted();

                    minimal_OBB.vertex = qi.Transform(vec(x1,y1,z1));
                    minimal_OBB.x_vec = qi.Transform(vec(x2,y1,z1)) - minimal_OBB.vertex;
                    minimal_OBB.y_vec = qi.Transform(vec(x1,y2,z1)) - minimal_OBB.vertex;
                    minimal_OBB.z_vec = qi.Transform(vec(x1,y1,z2)) - minimal_OBB.vertex;

                    minimal_OBB.volume = volume;
                    minimal_OBB.min_X = UHEL_X;
                    minimal_OBB.min_Y = UHEL_Y;
                    minimal_OBB.min_Z = UHEL_Z;

                    minimal_volume = volume;
                }

            }
        }
    }



    return minimal_OBB;

}

OBB_result OBB_cpu::calculate_OBB_Iteracni()
{

    // rozdil vysledku pri kterem skoncime iterovat
   float EPSILON = 10.0f;

   // na kolik dilku delime
   int granularita = 16;

   // stred pocatecniho uhlu
   double DELTA_UHEL = PI/2;

   // pocatecni min nalezeny objem
   float old_volume = std::numeric_limits<double>::infinity();

   // pocatecni vysledek
   OBB_result puvodni;
   puvodni.min_X = PI/2;
   puvodni.min_Y = PI/2;
   puvodni.min_Z = PI/2;
   puvodni.volume = std::numeric_limits<double>::infinity();

   return calculate_OBB_Iteracni_1iterace(puvodni,DELTA_UHEL,granularita,old_volume, EPSILON);


}

OBB_result OBB_cpu::calculate_OBB_Iteracni_1iterace(OBB_result old, float old_DELTA_UHEL, int granularita, float old_volume , float EPSILON)
{

    // tohle se bude vracet
    OBB_result minimal_OBB = old;
    float reset_UHEL_X = old.min_X - (old_DELTA_UHEL/2);
    float reset_UHEL_Y = old.min_Y - (old_DELTA_UHEL/2);
    float reset_UHEL_Z = old.min_Z - (old_DELTA_UHEL/2);

    float DELTA_UHEL = old_DELTA_UHEL / granularita;

    // pocatecni nejvetsi nalezeny objem = nekonecno
    double minimal_volume = old_volume;

    // postupne otacim vsechny body ruzne uhly natoceni a hledam, ktera pozice je nejvyhodnejsi
    float UHEL_X = reset_UHEL_X;
    for (int x=0; x<granularita; x++)
    {
        float UHEL_Y = reset_UHEL_Y;
        for (int y=0; y<granularita; y++)
        {
            float UHEL_Z = reset_UHEL_Z;
            for (int z=0; z<granularita; z++)
            {

                Quat q = Quat::FromEulerXYZ(UHEL_X, UHEL_Y, UHEL_Z);
                Polyhedron rotated_convex_hull (convex_hull);
                rotated_convex_hull.Transform(q);

                // najdeme indexy krajnich bodu
                int x1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 1.0f,  0.0f,  0.0f));
                int x2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC(-1.0f,  0.0f,  0.0f));
                int y1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  1.0f,  0.0f));
                int y2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f, -1.0f,  0.0f));
                int z1_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f,  1.0f));
                int z2_index = rotated_convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f, -1.0f));

                // nejvzdalenejsi souradnice do obou smeru vsech os
                float x1 = rotated_convex_hull.Vertex(x1_index).x;
                float x2 = rotated_convex_hull.Vertex(x2_index).x;
                float y1 = rotated_convex_hull.Vertex(y1_index).y;
                float y2 = rotated_convex_hull.Vertex(y2_index).y;
                float z1 = rotated_convex_hull.Vertex(z1_index).z;
                float z2 = rotated_convex_hull.Vertex(z2_index).z;

                // aktualni objem
                float volume = (std::abs(x1) + std::abs(x2)) * (std::abs(y1) + std::abs(y2)) * (std::abs(z1) + std::abs(z2));

                if (volume < minimal_volume)
                {
                    //std::cout << "Nalezen novy nejmensi objem x=" << x << "\ty=" << y << "\tz=" << z << "\tV=" << volume << "\tUHEL_X=" << UHEL_X/PI*180 << "\tUHEL_Y=" << UHEL_Y/PI*180 << "\tUHEL_Z=" << UHEL_Z/PI*180 << std::endl;

                    const Quat qi = q.Inverted();

                    minimal_OBB.vertex = qi.Transform(vec(x1,y1,z1));
                    minimal_OBB.x_vec = qi.Transform(vec(x2,y1,z1)) - minimal_OBB.vertex;
                    minimal_OBB.y_vec = qi.Transform(vec(x1,y2,z1)) - minimal_OBB.vertex;
                    minimal_OBB.z_vec = qi.Transform(vec(x1,y1,z2)) - minimal_OBB.vertex;

                    minimal_OBB.volume = volume;
                    minimal_OBB.min_X = UHEL_X;
                    minimal_OBB.min_Y = UHEL_Y;
                    minimal_OBB.min_Z = UHEL_Z;

                    minimal_volume = volume;
                }

                UHEL_Z += DELTA_UHEL;
            }
            UHEL_Y += DELTA_UHEL;
        }
        UHEL_X += DELTA_UHEL;
    }

    if ((std::abs(minimal_OBB.volume - old_volume) < EPSILON) || (DELTA_UHEL < 0.0001))
    {
        //std::cout << "koncime s inception, Abs=" << abs(minimal_OBB.volume - old_volume) << "   DELTA_UHEL=" << DELTA_UHEL << std::endl;
        return minimal_OBB;
    }
    else
    {

        //std::cout << "-------------------- DALSI iterace --------------------------" << std::endl;

        OBB_result inception =  calculate_OBB_Iteracni_1iterace(minimal_OBB,
                                             DELTA_UHEL,
                                             granularita,
                                             minimal_volume,
                                             EPSILON);

        if (inception.volume < minimal_volume)
        {
            //std::cout << "vracime inception hodnotu " << inception.volume << std::endl;
            return  inception;
        }
        else
        {
            //std::cout << "vracime puvodni hodnotu" << minimal_volume << std::endl;
            return  minimal_OBB;
        }

    }


}

OBB_result OBB_cpu::calculate_AABB_Bruteforce()
{

    /*
    int num_hull_vertices = convex_hull.NumVertices();

    for (int j=0; j<num_hull_vertices; j++)
    {
        std::cout << "bod "<< j <<  "\tx=" << convex_hull.Vertex(j).x << "\t y=" << convex_hull.Vertex(j).y << "\t z=" << convex_hull.Vertex(j).z << std::endl;
    }
    */

    // tohle se bude vracet
    OBB_result minimal_OBB;

    // najdeme indexy krajnich bodu
    int x1_index = convex_hull.ExtremeVertex(POINT_VEC( 1.0f,  0.0f,  0.0f));
    int x2_index = convex_hull.ExtremeVertex(POINT_VEC(-1.0f,  0.0f,  0.0f));
    int y1_index = convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  1.0f,  0.0f));
    int y2_index = convex_hull.ExtremeVertex(POINT_VEC( 0.0f, -1.0f,  0.0f));
    int z1_index = convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f,  1.0f));
    int z2_index = convex_hull.ExtremeVertex(POINT_VEC( 0.0f,  0.0f, -1.0f));

    float x1 = convex_hull.Vertex(x1_index).x;
    float x2 = convex_hull.Vertex(x2_index).x;
    float y1 = convex_hull.Vertex(y1_index).y;
    float y2 = convex_hull.Vertex(y2_index).y;
    float z1 = convex_hull.Vertex(z1_index).z;
    float z2 = convex_hull.Vertex(z2_index).z;

    float volume = (std::abs(x1) + std::abs(x2)) * (std::abs(y1) + std::abs(y2)) * (std::abs(z1) + std::abs(z2));

    minimal_OBB.vertex = vec(x1,y1,z1);
    minimal_OBB.x_vec = vec(x2,y1,z1) - minimal_OBB.vertex;
    minimal_OBB.y_vec = vec(x1,y2,z1) - minimal_OBB.vertex;
    minimal_OBB.z_vec = vec(x1,y1,z2) - minimal_OBB.vertex;

    minimal_OBB.volume = volume;
    minimal_OBB.min_X = 0.0f;
    minimal_OBB.min_Y = 0.0f;
    minimal_OBB.min_Z = 0.0f;

    return minimal_OBB;


}


