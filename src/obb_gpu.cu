#include <stdio.h>
#include <cstdlib>
#include <iostream>

#include <float.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_profiler_api.h>   // na mereni casu

#include "obb_gpu.h"
#include "unified_math_cuda.h"

typedef struct float4 cudafloat4;

struct OBB_result_GPU {
    cudafloat4 vertex;
    cudafloat4 x_vec, y_vec, z_vec;
    float volume;
};
typedef OBB_result_GPU OBB_result_GPU;

glm::vec3 float4_to_gmlvec3(cudafloat4 v)
{
    glm::vec3 vektor(v.x,v.y,v.z);
    return vektor;
}

inline __host__ __device__ cudafloat4 operator-(cudafloat4 a, cudafloat4 b)
{
    return make_float4(a.x - b.x, a.y - b.y, a.z - b.z,  a.w - b.w);
}

// float4, protoze u textury najde pouzit float3
texture<cudafloat4, 1, cudaReadModeElementType> tex_convex_hull_ref;	// na pravidla pro langtona


using namespace std;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// kernel - 1 vlakno orotuje vsechny vrcholy tou samou rotaci a udela objem z nejextremnejsich bodu v osach x,y,z
///        - min redukci do jednoho vlakna dostanu nejmensi vysledek z bloku
///        - ten se ulozi do globalni pameti, kde cpu vzbere nejlepsi vysledek ze vsech bloku

__global__ void kernel_obb_bruteforce_texture1d (int pocet_vrcholu, OBB_result_GPU *vysledky_device)
{

    const int threads_in_block = blockDim.x * blockDim.y;
    const int tid = blockDim.x * threadIdx.y + threadIdx.x;

    // sdilena pamet pro min redukci
    // dynamicky alokovana az pri startu kernelu
    extern __shared__ float array[];
    float* shared_volume = (float*)&array;
    int*   shared_tid =   (int*)&shared_volume[threads_in_block];
    shared_volume[tid] = -1.0f;

    float x_min = FLT_MAX;
    float x_max = FLT_MIN;
    float y_min = FLT_MAX;
    float y_max = FLT_MIN;
    float z_min = FLT_MAX;
    float z_max = FLT_MIN;

    // ziskame cisla od -1 do 1, quaternion bude tedy uz dopredu normalizovany
    const float X= -1.0f + threadIdx.x*(2.0f/blockDim.x);
    const float Y= -1.0f + threadIdx.y*(2.0f/blockDim.y);
    const float Z= -1.0f + blockIdx.x*(2.0f/gridDim.x);

    const float d = 1.0f - X*X - Y*Y - Z*Z;
    const float W = (d > 0.0f) ? sqrt(d) : 0.0f;

    // quaternion a inverzni quaternion
    const cudafloat4 q = make_float4(X, Y, Z, W);
    const cudafloat4 qi = make_float4(-X, -Y, -Z, W);

    /// orotuju vsechny vrcholy stejnou rotaci
    for (int address=0; address< pocet_vrcholu; address++)
    {
        // originalni vektor
        cudafloat4 vec = tex1Dfetch(tex_convex_hull_ref, address);

        // orotovany vektor
        cudafloat4 vec_rotated = quaternionMul(quaternionMul(q,vec),qi);

        // !!!!! THREAD DIVERGENCE JAK PRASE !!!!!
        // ale asi s tim nejde nic udelat
        if (vec_rotated.x < x_min) x_min = vec_rotated.x;
        if (vec_rotated.x > x_max) x_max = vec_rotated.x;
        if (vec_rotated.y < y_min) y_min = vec_rotated.y;
        if (vec_rotated.y > y_max) y_max = vec_rotated.y;
        if (vec_rotated.z < z_min) z_min = vec_rotated.z;
        if (vec_rotated.z > z_max) z_max = vec_rotated.z;

    }

    // lokalni volume
    const float volume = (abs(x_min) + abs(x_max)) * (abs(y_min) + abs(y_max)) * (abs(z_min) + abs(z_max));

    // pocatecni hodnotz pro min redukci
    shared_volume[tid] = volume;
    shared_tid[tid] = tid;

    // vsechny thready z bloku musi mit zapsany svuj objem do shared memory
    __syncthreads();

    // z celeho bloku vybereme nejmensi objem
    // min redukce, vzsledek najdeme na pozici 0
    for (unsigned int s=threads_in_block/2; s>0; s>>=1)
    {
        if (tid < s)
        {
            float minimum = min(shared_volume[tid], shared_volume[tid + s]);
            shared_volume[tid] = minimum;
            shared_tid[tid] = (minimum < shared_volume[tid+s]) ? shared_tid[tid] : shared_tid[tid+s];
        }
        __syncthreads();
    }


    // nejlepsi souradnice ma vlakno, jehoz index probublal na pozici shared_tid[0]
    if (tid == shared_tid[0])
    {        

        OBB_result_GPU vysledek;
        vysledek.volume = volume;
        vysledek.vertex = qtRotate(qi, make_float4(x_min, y_min, z_min, 0.0f));
        vysledek.x_vec = qtRotate(qi, make_float4(x_max, y_min, z_min, 0.0f)) - vysledek.vertex ;
        vysledek.y_vec = qtRotate(qi, make_float4(x_min, y_max, z_min, 0.0f)) - vysledek.vertex ;
        vysledek.z_vec = qtRotate(qi, make_float4(x_min, y_min, z_max, 0.0f)) - vysledek.vertex ;

        // vysledek se zapise do globalni pameti
        vysledky_device[blockIdx.x] = vysledek;
    }

}



/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// wrapper funkce ktera je volana z GMU.cpp

std::vector<glm::vec3> compute_OBB_Bruteforce_GPU(std::vector<glm::vec3> convex_hull_cpu, int pocet_vrcholu,  int granularita)
{

    printf ("Pocet vrcholu je %d \n",pocet_vrcholu);

    cudafloat4 *convex_hull_host = NULL;
    gpuErrchk(  cudaHostAlloc((void**) &convex_hull_host, (pocet_vrcholu*sizeof(cudafloat4)), cudaHostAllocDefault) );

    for (int i=0; i<pocet_vrcholu; i++)
    {
        convex_hull_host[i].x = convex_hull_cpu[i].x;
        convex_hull_host[i].y = convex_hull_cpu[i].y;
        convex_hull_host[i].z = convex_hull_cpu[i].z;
        convex_hull_host[i].w = -1.0f;
    }


    /// kopirovani do zarizeni a bindovani textur ---------------------------------------------
    cudafloat4 *convex_hull_device = NULL;
    gpuErrchk(  cudaMalloc((void**) &convex_hull_device, pocet_vrcholu *sizeof(cudafloat4)) );	//allocate the matrixes on the GPU memory
    gpuErrchk(  cudaMemcpy(convex_hull_device, convex_hull_host, pocet_vrcholu *sizeof(cudafloat4), cudaMemcpyHostToDevice) );
    gpuErrchk(  cudaBindTexture(NULL, tex_convex_hull_ref, convex_hull_device, pocet_vrcholu *sizeof(cudafloat4)) );


    /// alokace mista pro vysledky - 1 misto pro kazdy blok ----------------------------------
    OBB_result_GPU *vysledky_device = NULL;
    gpuErrchk(  cudaMalloc((void**) &vysledky_device, granularita *sizeof(OBB_result_GPU)) );	//allocate the matrixes on the GPU memory
    OBB_result_GPU *vysledky_host = NULL;
    gpuErrchk(  cudaMallocHost((void**) &vysledky_host, (granularita*sizeof(OBB_result_GPU))) ); // pinned memory



    /// zacatek mereni casu ---------------------------------------------
    float exec_time=0.0f;
    cudaEvent_t beginEvent;
    cudaEvent_t endEvent;
    gpuErrchk(  cudaEventCreate( &beginEvent ) );
    gpuErrchk(  cudaEventCreate( &endEvent ) );
    gpuErrchk(  cudaEventRecord( beginEvent, 0 ) );
    gpuErrchk(  cudaProfilerStart() );


    /// parametry spousteni ---------------------------------------------
    gpuErrchk(  cudaFuncSetCacheConfig(kernel_obb_bruteforce_texture1d,cudaFuncCachePreferL1) );  // preferujeme L1 cache nad sdilenou pameti
    int DIM_BLOCK_X = granularita;
    int DIM_BLOCK_Y = granularita;
    int SIZE_X = granularita;
    dim3 dimBlock(DIM_BLOCK_X, DIM_BLOCK_Y);	              // pocty threadu v bloku
    dim3 dimGrid(SIZE_X);     // pocty bloku v jednotlivych dimenzich
    //std::cout << "DIM_BLOCK_X="  << DIM_BLOCK_X << " DIM_BLOCK_Y=" << DIM_BLOCK_Y << " DIM_GRID_X=" << SIZE_X << std::endl;


    /// zavola se kernel ----------------------------------------------------------------
    kernel_obb_bruteforce_texture1d <<<dimGrid, dimBlock, (DIM_BLOCK_X*DIM_BLOCK_Y)*(sizeof(float)+sizeof(int)) >>> (pocet_vrcholu, vysledky_device);


    /// vyber nejlepsiho vysledku ze vsech bloku ------------------------------------------
    gpuErrchk (cudaMemcpy(vysledky_host, vysledky_device, granularita*sizeof(OBB_result_GPU), cudaMemcpyDeviceToHost));
    float min_volume = FLT_MAX;
    int min_volume_index = -1;
    for (int i=0; i<granularita; i++)
    {
        if (vysledky_host[i].volume < min_volume)
        {
            min_volume = vysledky_host[i].volume;
            min_volume_index = i;
        }
    }
    OBB_result_GPU minimal_OBB_tmp = vysledky_host[min_volume_index];

    /// konec mereni casu --------------------------------------------------------------
    gpuErrchk(  cudaProfilerStop() );
    gpuErrchk(  cudaEventRecord( endEvent, 0 ) );
    gpuErrchk(  cudaEventSynchronize( endEvent ) );
    gpuErrchk(  cudaEventElapsedTime( &exec_time, beginEvent, endEvent ) );
    gpuErrchk(  cudaEventDestroy( beginEvent ) );
    gpuErrchk(  cudaEventDestroy( endEvent ) );    
    cout << "Cas vypoctu: " << exec_time << "ms" << endl;
    cout << "nejmensi objem: " << minimal_OBB_tmp.volume << endl;


    /// free --------------------------------------------------------------------------
    gpuErrchk(  cudaUnbindTexture(tex_convex_hull_ref) );
    gpuErrchk(  cudaFree(convex_hull_device) );
    gpuErrchk(  cudaFree(vysledky_device) );
    gpuErrchk(  cudaFreeHost(convex_hull_host) );
    gpuErrchk(  cudaFreeHost(vysledky_host) );


    /// prevedeme do glm::vec3 ----------------------------------------------------------
    vector<glm::vec3> minimal_OBB;
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.vertex));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.x_vec));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.y_vec));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.z_vec));
    return minimal_OBB;

}

















__device__ static float atomicMax(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

__device__ static float atomicMin(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fminf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}


struct minmaxXYZ{
    float minx, maxx, miny, maxy, minz, maxz;
};
typedef minmaxXYZ minmaxXYZ;

/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// kernel - 1 vlakno = 1 rotace pro 1 bod
///        - je tolik vlaken jako je vrcholu, kazde vlakno ma 1 vrchol
///        - ten orotuje a nasledne se vyberou extremni body pomoci redukci
///        - nej body se poslou do cpu kde se zpracuji
///        - nad timto kernelem bezi 3 zanorene for cykly pres osy x,y,z

__global__ void kernel_obb_bruteforce2_texture1d (int pocet_vrcholu, float X, float Y, float Z, minmaxXYZ *vysledky_device)
{

    extern __shared__ float array[];
    float* shared_minx = (float*)&array;
    float* shared_maxx = (float*)&array[blockDim.x];
    float* shared_miny = (float*)&array[2*blockDim.x];
    float* shared_maxy = (float*)&array[3*blockDim.x];
    float* shared_minz = (float*)&array[4*blockDim.x];
    float* shared_maxz = (float*)&array[5*blockDim.x];

    const int tid = threadIdx.x;
    const int gid = (blockDim.x * blockIdx.x) + tid;


    cudafloat4 vec = tex1Dfetch(tex_convex_hull_ref, gid);

    const float d = 1.f - X*X - Y*Y - Z*Z;
    const float W = (d > 0.f) ? sqrt(d) : 0.f;
    const cudafloat4 q = make_float4(X, Y, Z, W);

    cudafloat4 vec_rotated = qtRotate(q,vec);


//    if (blockIdx.x==0)
//    {
//        printf ("vec thread=%d  %f   %f   %f \n", threadIdx.x, vec.x, vec.y, vec.z);
//        printf ("vec rotated thread=%d  %f   %f   %f \n", threadIdx.x, vec_rotated.x, vec_rotated.y, vec_rotated.z);
//    }


    shared_minx[tid] = FLT_MAX;
    shared_maxx[tid] = FLT_MIN;
    shared_miny[tid] = FLT_MAX;
    shared_maxy[tid] = FLT_MIN;
    shared_minz[tid] = FLT_MAX;
    shared_maxz[tid] = FLT_MIN;

    if (gid < pocet_vrcholu)
    {
        shared_minx[tid] = vec_rotated.x;
        shared_maxx[tid] = vec_rotated.x;
        shared_miny[tid] = vec_rotated.y;
        shared_maxy[tid] = vec_rotated.y;
        shared_minz[tid] = vec_rotated.z;
        shared_maxz[tid] = vec_rotated.z;
    }
    __syncthreads();

    /*
    if (blockIdx.x==0)
        printf ("vec thread=%d %f   %f   %f   %f   %f   %f \n",threadIdx.x, shared_minx[tid], shared_maxx[tid],shared_miny[tid], shared_maxy[tid],shared_minz[tid], shared_maxz[tid] );
        */



    for (unsigned int s=blockDim.x/2; s>0; s>>=1)
    {
        if (tid < s && gid < pocet_vrcholu)
        {
            // x
            shared_minx[tid] = min(shared_minx[tid], shared_minx[tid + s]);
            shared_maxx[tid] = max(shared_maxx[tid], shared_maxx[tid + s]);

            // y
            shared_miny[tid] = min(shared_miny[tid], shared_miny[tid + s]);
            shared_maxy[tid] = max(shared_maxy[tid], shared_maxy[tid + s]);

            // z
            shared_minz[tid] = min(shared_minz[tid], shared_minz[tid + s]);
            shared_maxz[tid] = max(shared_maxz[tid], shared_maxz[tid + s]);

        }
        __syncthreads();

    }

    if (tid == 0)
    {
        atomicMin(&(vysledky_device->minx), shared_minx[0]);
        atomicMax(&(vysledky_device->maxx), shared_maxx[0]);

        atomicMin(&(vysledky_device->miny), shared_miny[0]);
        atomicMax(&(vysledky_device->maxy), shared_maxy[0]);

        atomicMin(&(vysledky_device->minz), shared_minz[0]);
        atomicMax(&(vysledky_device->maxz), shared_maxz[0]);

        //printf ("volume block=%d %f   %f   %f   %f   %f   %f \n",blockIdx.x, shared_minx[0], shared_maxx[0],shared_miny[0], shared_maxy[0],shared_minz[0], shared_maxz[0] );

    }

}



/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// wrapper funkce ktera je volana z GMU.cpp

std::vector<glm::vec3> compute_OBB_Bruteforce_GPU2(std::vector<glm::vec3> convex_hull_cpu, int pocet_vrcholu,  int granularita)
{

    printf ("Pocet vrcholu je %d \n",pocet_vrcholu);

    /// prevedeni bodu do formatu pro gpu -----------------------------------------------------------
    cudafloat4 *convex_hull_host = NULL;
    gpuErrchk(  cudaHostAlloc((void**) &convex_hull_host, (pocet_vrcholu*sizeof(cudafloat4)), cudaHostAllocDefault) );
    for (int i=0; i<pocet_vrcholu; i++)
    {
        convex_hull_host[i].x = convex_hull_cpu[i].x;
        convex_hull_host[i].y = convex_hull_cpu[i].y;
        convex_hull_host[i].z = convex_hull_cpu[i].z;
        convex_hull_host[i].w = -1.0f;
    }

    /// kopirovani bodu do zarizeni a bindovani textur ---------------------------------------------
    cudafloat4 *convex_hull_device = NULL;
    gpuErrchk(  cudaMalloc((void**) &convex_hull_device, pocet_vrcholu *sizeof(cudafloat4)) );	//allocate the matrixes on the GPU memory
    gpuErrchk(  cudaMemcpy(convex_hull_device, convex_hull_host, pocet_vrcholu *sizeof(cudafloat4), cudaMemcpyHostToDevice) );
    gpuErrchk(  cudaBindTexture(NULL, tex_convex_hull_ref, convex_hull_device, pocet_vrcholu *sizeof(cudafloat4)) );


    /// pinned memory pro defaultni vysledky ------------------------------------------------------
    minmaxXYZ *vysledky_host_default = NULL;
    gpuErrchk ( cudaMallocHost((void**)&vysledky_host_default, sizeof(minmaxXYZ)) );
    vysledky_host_default->minx = FLT_MAX;
    vysledky_host_default->maxx = FLT_MIN;
    vysledky_host_default->miny = FLT_MAX;
    vysledky_host_default->maxy = FLT_MIN;
    vysledky_host_default->minz = FLT_MAX;
    vysledky_host_default->maxz = FLT_MIN;


    /// alokace mista pro vysledky v device - 1 misto pro kazdy stream ----------------------------------
    minmaxXYZ *vysledky_device = NULL;
    gpuErrchk(  cudaMalloc((void**) &vysledky_device, 2*sizeof(minmaxXYZ)) );	//allocate the matrixes on the GPU memory
    gpuErrchk (cudaMemcpy(&(vysledky_device[0]), &vysledky_host_default, sizeof(minmaxXYZ), cudaMemcpyHostToDevice));
    gpuErrchk (cudaMemcpy(&(vysledky_device[1]), &vysledky_host_default, sizeof(minmaxXYZ), cudaMemcpyHostToDevice));


    /// alokovani pinned memory pro vysledky v hostu - 2ks, 1 pro kazdy stream ----------------------------
    minmaxXYZ *vysledky_host;
    gpuErrchk ( cudaMallocHost((void**)&vysledky_host, 2*sizeof(minmaxXYZ)) );


    /// zacatek mereni casu ---------------------------------------------
    float exec_time=0.0f;
    cudaEvent_t beginEvent;
    cudaEvent_t endEvent;
    gpuErrchk(  cudaEventCreate( &beginEvent ) );
    gpuErrchk(  cudaEventCreate( &endEvent ) );
    gpuErrchk(  cudaEventRecord( beginEvent, 0 ) );
    gpuErrchk(  cudaProfilerStart() );


    /// parametry spousteni ---------------------------------------------
    gpuErrchk(  cudaFuncSetCacheConfig(kernel_obb_bruteforce2_texture1d,cudaFuncCachePreferShared) );  // preferujeme L1 cache nad sdilenou pameti
    int DIM_BLOCK_X = 128;
    int DIM_GRID_X = pocet_vrcholu / DIM_BLOCK_X + (pocet_vrcholu%DIM_BLOCK_X != 0);
    dim3 dimBlock(DIM_BLOCK_X);	  // pocty threadu v bloku
    dim3 dimGrid(DIM_GRID_X);     // pocty bloku v jednotlivych dimenzich
    std::cout << "DIM_BLOCK_X="  << DIM_BLOCK_X <<  " DIM_GRID_X=" << DIM_GRID_X << std::endl;


    /// 2 streamy ----------------------------------------------------------
    cudaStream_t stream[2];
    gpuErrchk (cudaStreamCreate(&stream[0]));
    gpuErrchk (cudaStreamCreate(&stream[1]));

    const float DELTA = 2.0/granularita;
    float min_volume = FLT_MAX;
    float volume;
    OBB_result_GPU minimal_OBB_tmp;

    bool prvni_spusteni = true;

    float Z = -1.0;
    float Y;
    float X;

    for (int z=0; z< granularita; z++)
    {
        Y= -1.0;
        for (int y=0; y< granularita; y++)
        {
            X= -1.0;
            for (int x=0; x< granularita; x++)
            {

                /// zavola se vsechno pro stream [(x+1)%2] ------------------------------------------
                // zavola se kernel
                kernel_obb_bruteforce2_texture1d <<<dimGrid, dimBlock, DIM_BLOCK_X*sizeof(float)*6 , stream[(x+1)%2]>>> (pocet_vrcholu, X, Y, Z, &(vysledky_device[(x+1)%2]));
                // zkopirujeme zpet vysledek
                gpuErrchk (cudaMemcpyAsync(&vysledky_host[(x+1)%2], &(vysledky_device[(x+1)%2]), sizeof(minmaxXYZ), cudaMemcpyDeviceToHost, stream[(x+1)%2]));
                // vycistime pamet pro dalsi kolo
                gpuErrchk (cudaMemcpyAsync(&(vysledky_device[(x+1)%2]), &vysledky_host_default, sizeof(minmaxXYZ), cudaMemcpyHostToDevice, stream[(x+1)%2]));


                /// v prvnim spusteni nemam co vyhodnocovat -> preskakuju ---------------------------
                if (prvni_spusteni)
                {
                    prvni_spusteni = false;
                    X+= DELTA;
                    continue;
                }

                /// vyhodnoceni streamu [x%2] -------------------------------------------------------
                // pocka se az se dokonci
                cudaStreamSynchronize(stream[x%2]);
                // spocteme objem
                volume = (abs(vysledky_host[x%2].minx) + abs(vysledky_host[x%2].maxx)) *
                         (abs(vysledky_host[x%2].miny) + abs(vysledky_host[x%2].maxy)) *
                         (abs(vysledky_host[x%2].minz) + abs(vysledky_host[x%2].maxz));
                //std::cout << "volume " << " minx" << vysledky_host.minx << " maxx" << vysledky_host.maxx  << " miny" << vysledky_host.miny << " maxy" << vysledky_host.maxy  << " minz" << vysledky_host.minz << " maxz" << vysledky_host.maxz << std::endl;
                // prepiseme stary
                if (volume < min_volume)
                {
                    min_volume = volume;
                    float X_;
                    if (X == 0.0f) // X_ je X z minule iterace
                        X_ = 1.0;
                    else
                        X_ = X-DELTA;
                    const float d = 1.f - X_*X_ - Y*Y - Z*Z;
                    const float W = (d > 0.f) ? sqrt(d) : 0.f;
                    const cudafloat4 qi = make_float4(-X_, -Y, -Z, W);
                    minimal_OBB_tmp.volume = volume;
                    minimal_OBB_tmp.vertex = qtRotate(qi, make_float4(vysledky_host[x%2].minx, vysledky_host[x%2].miny, vysledky_host[x%2].minz, 0.0f));
                    minimal_OBB_tmp.x_vec = qtRotate(qi, make_float4(vysledky_host[x%2].maxx, vysledky_host[x%2].miny, vysledky_host[x%2].minz, 0.0f)) - minimal_OBB_tmp.vertex ;
                    minimal_OBB_tmp.y_vec = qtRotate(qi, make_float4(vysledky_host[x%2].minx, vysledky_host[x%2].maxy, vysledky_host[x%2].minz, 0.0f)) - minimal_OBB_tmp.vertex ;
                    minimal_OBB_tmp.z_vec = qtRotate(qi, make_float4(vysledky_host[x%2].minx, vysledky_host[x%2].miny, vysledky_host[x%2].maxz, 0.0f)) - minimal_OBB_tmp.vertex ;
                }

                X+= DELTA;

            }

            Y+= DELTA;
        }

        Z+= DELTA;
    }


    /// dovyhodnoti se posledni kolo se streamem 0 ------------------------------------------------
    cudaStreamSynchronize(stream[0]);
    volume = (abs(vysledky_host[0].minx) + abs(vysledky_host[0].maxx)) *
             (abs(vysledky_host[0].miny) + abs(vysledky_host[0].maxy)) *
             (abs(vysledky_host[0].minz) + abs(vysledky_host[0].maxz));
    if (volume < min_volume)
    {
        min_volume = volume;
        const cudafloat4 qi = make_float4(-1.0, -1.0, -1.0, 0.0f);
        minimal_OBB_tmp.volume = volume;
        minimal_OBB_tmp.vertex = qtRotate(qi, make_float4(vysledky_host[0].minx, vysledky_host[0].miny, vysledky_host[0].minz, 0.0f));
        minimal_OBB_tmp.x_vec = qtRotate(qi, make_float4(vysledky_host[0].maxx, vysledky_host[0].miny, vysledky_host[0].minz, 0.0f)) - minimal_OBB_tmp.vertex ;
        minimal_OBB_tmp.y_vec = qtRotate(qi, make_float4(vysledky_host[0].minx, vysledky_host[0].maxy, vysledky_host[0].minz, 0.0f)) - minimal_OBB_tmp.vertex ;
        minimal_OBB_tmp.z_vec = qtRotate(qi, make_float4(vysledky_host[0].minx, vysledky_host[0].miny, vysledky_host[0].maxz, 0.0f)) - minimal_OBB_tmp.vertex ;
    }




    /// konec mereni casu --------------------------------------------------------------
    gpuErrchk(  cudaProfilerStop() );
    gpuErrchk(  cudaEventRecord( endEvent, 0 ) );
    gpuErrchk(  cudaEventSynchronize( endEvent ) );
    gpuErrchk(  cudaEventElapsedTime( &exec_time, beginEvent, endEvent ) );
    gpuErrchk(  cudaEventDestroy( beginEvent ) );
    gpuErrchk(  cudaEventDestroy( endEvent ) );
    cout << "Cas vypoctu: " << exec_time << "ms" << endl;
    cout << "nejmensi objem: " << min_volume << endl;


    /// free --------------------------------------------------------------------------
    gpuErrchk(  cudaUnbindTexture(tex_convex_hull_ref) );
    gpuErrchk(  cudaFree(convex_hull_device) );
    gpuErrchk(  cudaFree(vysledky_device) );
    gpuErrchk(  cudaFreeHost(convex_hull_host) );
    gpuErrchk(  cudaStreamDestroy(stream[0]) );
    gpuErrchk(  cudaStreamDestroy(stream[1]) );
    gpuErrchk(  cudaFreeHost(vysledky_host) );


    /// prevedeme do glm::vec3 ----------------------------------------------------------
    vector<glm::vec3> minimal_OBB;
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.vertex));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.x_vec));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.y_vec));
    minimal_OBB.push_back( float4_to_gmlvec3(minimal_OBB_tmp.z_vec));
    return minimal_OBB;

}

