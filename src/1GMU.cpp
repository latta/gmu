#include "GMU.h"

#include <algorithm>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

//#include "bunny.h"

using namespace std;

GMU::GMU(std::vector<glm::vec3> inPts, std::vector<glm::vec3> bodyPts)
{
    inPoints=inPts;
    bodyPoints=bodyPts;

	rmbPressed = false;
	lmbPressed = false;
	posx = 0, posy = 0;
	rotx = 170, roty = 25, zoom = 4;
	//rotx = 180, roty = 88, zoom = 1.22;
	
//	animationEnabled = false;
//	time = 0;
//	lastFrameTics = 0;
	wireframe = false;
	//debug = false;
	//fullscreen = true;	
}

void GMU::init() {
	/*
	* Vytvoreni shader programu
	*/

	/* Muzete zmenit podle potreby (linux/mingw)*/
	string prefix = "shaders/";

    // Kompilace shaderu
	vs = compileShader(GL_VERTEX_SHADER, loadFile(prefix + "GMU.vert"));
	fs = compileShader(GL_FRAGMENT_SHADER, loadFile(prefix + "GMU.frag"));
	program = linkShader(2, vs, fs);


	// zisakni indexu atributu
	positionAttrib = glGetAttribLocation(program, "position");
	//normalAttrib = glGetAttribLocation(program, "normal");


	mUniform = glGetUniformLocation(program, "m");
	vUniform = glGetUniformLocation(program, "v");
	pUniform = glGetUniformLocation(program, "p");
//	animationEnabledUniform = glGetUniformLocation(program, "animationEnabled");
//	timeUniform = glGetUniformLocation(program, "time");
	

	/*
	* Kopirovani dat na GPU
	*/

    // buffer with point to make axis
    glGenVertexArrays(1, &vaoAxes);
    glBindVertexArray(vaoAxes);
    float axes[] = {-1,  0,  0,
					1,  0,  0,
					0, -1,  0,
					0,  1,  0,
					0,  0, -1,
					0,  0,  1};
    glGenBuffers(1, &vboAxes);
    glBindBuffer(GL_ARRAY_BUFFER, vboAxes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(axes), axes, GL_STATIC_DRAW);

	glEnableVertexAttribArray(positionAttrib);
	glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (GLvoid*)(0));
	/*
	glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(float)*6, (GLvoid*)(0));
	
	
	
	glEnableVertexAttribArray(normalAttrib);
	glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (GLvoid*)(sizeof(float) * 3));
	/*


	/*
	* Vytvoreni bufferu pro kralika
	*/
	/*
	glGenVertexArrays(1, &vaoBunny);
	glBindVertexArray(vaoBunny);
	glGenBuffers(1, &vboBunny);
	glBindBuffer(GL_ARRAY_BUFFER, vboBunny);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bunnyVertices), bunnyVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &eboBunny);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboBunny);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(bunny), bunny, GL_STATIC_DRAW);

	glEnableVertexAttribArray(positionAttrib);
	glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(struct BunnyVertex), (GLvoid*)(offsetof(struct BunnyVertex, position)));
	glEnableVertexAttribArray(normalAttrib);
	glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(struct BunnyVertex), (GLvoid*)(offsetof(struct BunnyVertex, normal)));
	*/
}


void GMU::draw() {
	/* Aktualizace casu animace - klavesa A
	//Uint32 tics = SDL_GetTicks();
	//Uint32 dt = tics - lastFrameTics;
	//lastFrameTics = tics;
	if (animationEnabled) {
		time += dt;
	}
	* */

	/* Matice */
	updateMatrix();

	glViewport(0, 0, width, height);

	glClearColor(0.68, 0.88, 0.93, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);


	glUseProgram(program);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, glm::value_ptr(projection));

	//glUniform1i(timeUniform, time);
	//glUniform1i(animationEnabledUniform, animationEnabled);

	/* Nastaveni poctu kontrolnich bodu */
	//glPatchParameteri(GL_PATCH_VERTICES, 3);

	/* Prepinani na dratovy model - klavesa W*/
	if (wireframe) {
		glLineWidth(3.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}


    glBindVertexArray(vaoAxes);
	glDrawArrays(GL_LINES, 0, 6);

	
	//glDisableVertexAttribArray(0);
	
}

void GMU::onMouseMove(int dx, int dy, int x, int y){
	if (rmbPressed||lmbPressed) {
		rotx += dx;
		roty += dy;
		roty = max(min(roty, 89), -89);
	}
	posx = x;
	posy = y;
}

void GMU::onMousePress(Uint8 button, int x, int y){
	switch (button) {
	case SDL_BUTTON_LEFT:
		lmbPressed = true;
		break;
	case SDL_BUTTON_MIDDLE:
		break;
	case SDL_BUTTON_RIGHT:
		rmbPressed = true;
		break;
	}
}

void GMU::onMouseRelease(Uint8 button, int x, int y){
	switch (button) {
	case SDL_BUTTON_LEFT:
		lmbPressed = false;
		break;
	case SDL_BUTTON_MIDDLE:
		break;
	case SDL_BUTTON_RIGHT:
		rmbPressed = false;
		break;
	}
}

void GMU::onMouseWheel(int delta){
	if (delta > 0) {
		zoom /= 1.1;
	}else {
		zoom *= 1.1;
	}
}

void GMU::onKeyPress(SDL_Keycode key, Uint16 mod){
	switch (key) {
	case SDLK_ESCAPE:
		quit();
	case SDLK_a:
		animationEnabled = !animationEnabled;
		if (animationEnabled) time = 0;
		break;
	case SDLK_d:
		//debug = !debug;
		break;
	case SDLK_w:
		wireframe = !wireframe;
		break;
	}
}

void GMU::updateMatrix() {
	model = glm::mat4(1.0);

	float radx = glm::radians((float)rotx);
	float rady = glm::radians((float)roty);
	float x = zoom * cos(rady) * cos(radx);
	float y = zoom * sin(rady);
	float z = zoom * cos(rady) * sin(radx);

	glm::vec3 eye(x, y, z);
	glm::vec3 center(0, 0, 0);
	glm::vec3 up(0, 1, 0);
	view = glm::lookAt(eye, center, up);
	projection = glm::perspective(45.0f, (float)width / (float)height, 0.1f, 1000.0f);
}

int main(int /*argc*/, char ** /*argv*/)
{
	// get random points	
    vector<glm::vec3> inPts;
    inPts.push_back( glm::vec3(1,1,1));
    inPts.push_back( glm::vec3(1,1,1));
	// calculate bounding box

    vector<glm::vec3> bodyPts;
    bodyPts.push_back( glm::vec3(0,0,0));
    bodyPts.push_back( glm::vec3(2,0,0));
    bodyPts.push_back( glm::vec3(0,2,0));

    // run visualization
    GMU app(inPts, bodyPts);
    return app.run();
}


