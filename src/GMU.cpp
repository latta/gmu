#include <algorithm>
#include <chrono>
#include <getopt.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

#include "GMU.h"
#include "obb_gpu.h"
#include "obb_cpu.h"


using namespace std;

int cubeElements[] = {
    0,1,
    1,2,
    2,3,
    3,0,
    4,5,
    5,6,
    6,7,
    7,4,
    0,4,
    1,5,
    2,6,
    3,7
};

GMU::GMU(std::vector<glm::vec3> inPointsHull, std::vector<vec> inPoints, std::vector<glm::vec3> bodyPoints):
    inPointsHull(inPointsHull), inPoints(inPoints), bodyPoints(bodyPoints)
{

    enableHull = true;
	rmbPressed = false;
	lmbPressed = false;
	posx = 0, posy = 0;
	rotx = 170, roty = 25, zoom = 4;
	//rotx = 180, roty = 88, zoom = 1.22;
}


void GMU::init() {

	string prefix = "shaders/";

    // Kompilace shaderu
    vsA = compileShader(GL_VERTEX_SHADER, loadFile(prefix + "axis.vert"));
    fsA = compileShader(GL_FRAGMENT_SHADER, loadFile(prefix + "axis.frag"));
    programAxis = linkShader(2, vsA, fsA);


    vsP = compileShader(GL_VERTEX_SHADER, loadFile(prefix + "points.vert"));
    gsP = compileShader(GL_GEOMETRY_SHADER, loadFile(prefix + "points.geom"));
    fsP = compileShader(GL_FRAGMENT_SHADER, loadFile(prefix + "points.frag"));
    programPoints = linkShader(3, vsP, gsP, fsP);


	// zisakni indexu atributu
    positionAttribAxis = glGetAttribLocation(programAxis, "position");
    mUniform = glGetUniformLocation(programAxis, "m");
    vUniform = glGetUniformLocation(programAxis, "v");
    pUniform = glGetUniformLocation(programAxis, "p");


    positionAttribPoints = glGetAttribLocation(programPoints, "position");
    mUniformP = glGetUniformLocation(programPoints, "m");
    vUniformP = glGetUniformLocation(programPoints, "v");
    pUniformP = glGetUniformLocation(programPoints, "p");

    cubeEnabledUniform = glGetUniformLocation(programAxis, "cubeEnabled");


    //Kopirovani dat na GPU

    glGenVertexArrays(1, &vaoAxes);
    glBindVertexArray(vaoAxes);

    float axes[] = {-1,  0,  0,
					1,  0,  0,
					0, -1,  0,
					0,  1,  0,
					0,  0, -1,
					0,  0,  1};
    glGenBuffers(1, &vboAxes);
    glBindBuffer(GL_ARRAY_BUFFER, vboAxes);
    glBufferData(GL_ARRAY_BUFFER, sizeof(axes), axes, GL_STATIC_DRAW);

    glEnableVertexAttribArray(positionAttribAxis);
    glVertexAttribPointer(positionAttribAxis, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (GLvoid*)(0));


    // body pouze obalove teleso
    glGenVertexArrays(1, &vaoPointsHull);
    glBindVertexArray(vaoPointsHull);

    int inPtsSize = inPointsHull.size();
    float *points = (float*) malloc(inPtsSize * 3 * sizeof(float));
    for (unsigned int i=0; i < inPointsHull.size()*3; i+=3)
    {
        points[i]= inPointsHull[i/3].x;
        points[i+1]= inPointsHull[i/3].y;
        points[i+2]= inPointsHull[i/3].z;
    }

    glGenBuffers(1, &vboPointsHull);
    glBindBuffer(GL_ARRAY_BUFFER, vboPointsHull);
    glBufferData(GL_ARRAY_BUFFER, inPtsSize * 3 * sizeof(float), points, GL_STATIC_DRAW);

    glEnableVertexAttribArray(positionAttribPoints);
    glVertexAttribPointer(positionAttribPoints, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (GLvoid*)(0));


    // body vsechny
    glGenVertexArrays(1, &vaoPoints);
    glBindVertexArray(vaoPoints);

    int inPtsSize2 = inPoints.size();
    float *points2 = (float*) malloc(inPtsSize2 * 3 * sizeof(float));
    for (unsigned int i=0; i < inPoints.size()*3; i+=3)
    {
        points2[i]= inPoints[i/3].x;
        points2[i+1]= inPoints[i/3].y;
        points2[i+2]= inPoints[i/3].z;
    }

    glGenBuffers(1, &vboPoints);
    glBindBuffer(GL_ARRAY_BUFFER, vboPoints);
    glBufferData(GL_ARRAY_BUFFER, inPtsSize2 * 3 * sizeof(float), points2, GL_STATIC_DRAW);

    glEnableVertexAttribArray(positionAttribPoints);
    glVertexAttribPointer(positionAttribPoints, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (GLvoid*)(0));



    // krychle
    float cubeVertices[24]= {0};

    glm::vec3 pt=bodyPoints.at(0);
    glm::vec3 v1=bodyPoints.at(1);
    glm::vec3 v2=bodyPoints.at(2);
    glm::vec3 v3=bodyPoints.at(3);

    cubeVertices[0]= pt.x;
    cubeVertices[1]= pt.y;
    cubeVertices[2]= pt.z;
    cubeVertices[3]= pt.x + v1.x;
    cubeVertices[4]= pt.y + v1.y;
    cubeVertices[5]= pt.z + v1.z;
    cubeVertices[6]= pt.x + v1.x + v2.x;
    cubeVertices[7]= pt.y + v1.y + v2.y;
    cubeVertices[8]= pt.z + v1.z + v2.z;
    cubeVertices[9]= pt.x + v2.x;
    cubeVertices[10]= pt.y + v2.y;
    cubeVertices[11]= pt.z + v2.z;

    cubeVertices[12]= pt.x + v3.x;
    cubeVertices[13]= pt.y + v3.y;
    cubeVertices[14]= pt.z + v3.z;
    cubeVertices[15]= pt.x + v1.x + v3.x;
    cubeVertices[16]= pt.y + v1.y + v3.y;
    cubeVertices[17]= pt.z + v1.z + v3.z;
    cubeVertices[18]= pt.x + v1.x + v2.x + v3.x;
    cubeVertices[19]= pt.y + v1.y + v2.y + v3.y;
    cubeVertices[20]= pt.z + v1.z + v2.z + v3.z;
    cubeVertices[21]= pt.x + v2.x + v3.x;
    cubeVertices[22]= pt.y + v2.y + v3.y;
    cubeVertices[23]= pt.z + v2.z + v3.z;


    glGenVertexArrays(1, &vaoCube);
    glBindVertexArray(vaoCube);
    glGenBuffers(1, &vboCube);
    glBindBuffer(GL_ARRAY_BUFFER, vboCube);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

    glGenBuffers(1, &eboCube);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboCube);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeElements), cubeElements, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, vboCube);
    glEnableVertexAttribArray(positionAttribAxis);
    glVertexAttribPointer(positionAttribAxis, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (GLvoid*)(0));


    // textura bodu
    SDL_Surface* res_texture = IMG_Load("textures/point.png");
    if (res_texture == NULL) {
        cerr << "IMG_Load: " << SDL_GetError() << endl;
    }

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, // target
                 0,  // level, 0 = base, no minimap,
                 GL_RGBA, // internalformat
                 res_texture->w,  // width
                 res_texture->h,  // height
                 0,  // border, always 0 in OpenGL ES
                 GL_RGBA,  // format
                 GL_UNSIGNED_BYTE, // type
                 res_texture->pixels);
    SDL_FreeSurface(res_texture);

}


void GMU::draw()
{
	updateMatrix();

	glViewport(0, 0, width, height);

	glClearColor(0.68, 0.88, 0.93, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // antialiasing
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);


    // vykresleni os
    glUseProgram(programAxis);

    glUniform1i(cubeEnabledUniform, false);
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, glm::value_ptr(projection));

    glBindVertexArray(vaoAxes);
	glDrawArrays(GL_LINES, 0, 6);


    glUniform1i(cubeEnabledUniform, true);
    // vykresleni krychle
    glBindVertexArray(vaoCube);

    glLineWidth(2.0f);
    glDrawElements(GL_LINES, 36, GL_UNSIGNED_INT, (void *)(0));
    glLineWidth(1.0f);



    // vykresleni bodu
    glUseProgram(programPoints);

    glUniformMatrix4fv(mUniformP, 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(vUniformP, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(pUniformP, 1, GL_FALSE, glm::value_ptr(projection));

    if (enableHull)
    {
        glBindVertexArray(vaoPointsHull);
        glDrawArrays(GL_POINTS, 0, inPointsHull.size());
    }
    else
    {
        glBindVertexArray(vaoPoints);
        glDrawArrays(GL_POINTS, 0, inPoints.size());
    }

    glBindVertexArray(0);

}

void GMU::onMouseMove(int dx, int dy, int x, int y){
	if (rmbPressed||lmbPressed) {
		rotx += dx;
		roty += dy;
		roty = max(min(roty, 89), -89);
	}
	posx = x;
	posy = y;
}

void GMU::onMousePress(Uint8 button, int x, int y){
	switch (button) {
	case SDL_BUTTON_LEFT:
		lmbPressed = true;
		break;
	case SDL_BUTTON_MIDDLE:
		break;
	case SDL_BUTTON_RIGHT:
		rmbPressed = true;
		break;
	}
}

void GMU::onMouseRelease(Uint8 button, int x, int y){
	switch (button) {
	case SDL_BUTTON_LEFT:
		lmbPressed = false;
		break;
	case SDL_BUTTON_MIDDLE:
		break;
	case SDL_BUTTON_RIGHT:
		rmbPressed = false;
		break;
	}
}

void GMU::onMouseWheel(int delta){
	if (delta > 0) {
		zoom /= 1.1;
	}else {
		zoom *= 1.1;
	}
}

void GMU::onKeyPress(SDL_Keycode key, Uint16 mod){
	switch (key) {
	case SDLK_ESCAPE:
        quit();
		break;
    case SDLK_h:
        enableHull=!enableHull;
        break;
    }
}


void GMU::updateMatrix() {
	model = glm::mat4(1.0);

	float radx = glm::radians((float)rotx);
	float rady = glm::radians((float)roty);
	float x = zoom * cos(rady) * cos(radx);
	float y = zoom * sin(rady);
	float z = zoom * cos(rady) * sin(radx);

	glm::vec3 eye(x, y, z);
	glm::vec3 center(0, 0, 0);
	glm::vec3 up(0, 1, 0);
	view = glm::lookAt(eye, center, up);
	projection = glm::perspective(45.0f, (float)width / (float)height, 0.1f, 1000.0f);
}


/*
 * Funkce ke generovani nahodneho poctu bodu
*/
std::vector<vec> getRandomPoints(int n)
{
    std::vector<vec> points;
    vec v;
    for (int i=0; i<n ; i++)
    {
        // nahodne body z boxu
        LCG rng;
        double max = glm::linearRand(2.0, (n >= 100) ? ((n >= 1000) ? 20.0 : 15.0) : 10.0);

        if (glm::linearRand(0.0, 1.0) < 0.7 )
            v = Sphere::RandomPointInside(rng, vec(0,0,0), max);
        else
            v= Quat::RandomRotation(rng).Transform(vec::RandomBox(rng, -max*1.2, max*1.2));

        points.push_back(v);

    }
    return points;
}



void print_usage(){
    std::cout << "Usage:" << std::endl
              << "-r pocet, --random pocet        zdroj jsou nahodne body" << std::endl
              << "-s file, --source file          zdroj je soubor s body" << std::endl
              << "-v metoda, --visualize metoda   tato metoda bude spustena a vizualizovana" << std::endl
              << "--jmeno_metody                  tato metoda bude spustena" << std::endl
              << "-a, --runall                    spusti vsechny metody" << std::endl
              << "-h, --help                      zobrazi tento help" << std::endl << std::endl;

    std::cout << "Metody:" << std::endl
              << "cpu_bf -  Brute force metoda na CPU" << std::endl
              << "cpu_it -  Iteracni brute force metoda na CPU" << std::endl
              << "gpu_bf -  Brute force metoda na GPU - nejrychlejsi" << std::endl
              << "gpu_bf2 - 2. brute force metoda na GPU - pomalejsi" << std::endl
              << "cpu_lib - Knihovni metoda, ktera najde optimalni reseni (pomala)" << std::endl << std::endl;


}

int main(int argc, char ** argv)
{

    static int run_cpu_bf = 0;
    static int run_cpu_it = 0;
    static int run_gpu_bf = 0;
    static int run_gpu_bf2 = 0;
    static int run_cpu_lib = 0;

    bool visualise_cpu_bf = false;
    bool visualise_cpu_it = false;
    bool visualise_gpu_bf = false;
    bool visualise_gpu_bf2 = false;

    bool source_is_file = false;
    string input_file = "";

    bool source_is_random = false;
    int num_points_generated = 1000;

    int c;
    while (1)
    {
        static struct option long_options[] =
            {
                /* These options set a flag. */
            {"cpu_bf", no_argument,       &run_cpu_bf, 1},
            {"cpu_it",   no_argument,       &run_cpu_it, 1},
            {"gpu_bf", no_argument,       &run_gpu_bf, 1},
            {"gpu_bf2",   no_argument,       &run_gpu_bf2, 1},
            /* These options don’t set a flag.
                   We distinguish them by their indices. */
            {"visualize",     required_argument,       0, 'v'},
            {"source",  required_argument,       0, 's'},
            {"random",  required_argument, 0, 'r'},
            {"runall",  no_argument, 0, 'a'},
            {"help",  no_argument, 0, 'h'},
            {0, 0, 0, 0}
        };

        int option_index = 0;

        c = getopt_long (argc, argv, "v:s:r:ha",
                         long_options, &option_index);

        if (c == -1)
            break;

        switch (c) {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if (long_options[option_index].flag != 0)
                break;
            printf ("option %s", long_options[option_index].name);
            if (optarg)
                printf (" with arg %s", optarg);
            printf ("\n");
            break;
        case 'v': {
            std::string s=optarg;
            if (s == "cpu_bf") run_cpu_bf = visualise_cpu_bf = true;
            if (s == "cpu_it") run_cpu_it = visualise_cpu_it = true;
            if (s == "gpu_bf") run_gpu_bf = visualise_gpu_bf = true;
            if (s == "gpu_bf2") run_gpu_bf2 = visualise_gpu_bf2 = true;
            //std::cout << "vizualizace metodou " << optarg << std::endl;
            break;
        }
        case 's':{
            input_file = optarg;
            //std::cout << "zdroj bodu je soubor " << input_file << std::endl;
            source_is_random = false;
            source_is_file = true;
            break;
        }
        case 'r':{
            source_is_file = false;
            source_is_random = true;
            num_points_generated = atoi(optarg);
            //std::cout << "zdroj bodu je " << num_points_generated << " nahodnych bodu." << std::endl;
            break;
        }
        case 'a':{
            run_cpu_bf = run_cpu_it = run_gpu_bf = run_gpu_bf2 = run_cpu_lib = true;
            break;
        }
        case 'h':
            print_usage();
            break;
        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            std::cout << "nastavit default parametry" << std::endl;
            abort ();

        }
    }

    // BEZ PARAMETRU
    // ----------------------
    if (argc<=1)
    {
        std::cout << "!! Nebyly zadany zadne parametry, poustim s parametry 100 random bodu, metoda gpu-bf !!" << std::endl << std::endl;
        print_usage();
        source_is_random = true;
        num_points_generated = 100;
        run_gpu_bf = true;
        visualise_gpu_bf = true;
    }

    // GENEROVANI BODU
    // ----------------------
    OBB_cpu *moje_obb = NULL;

    std::vector<vec> inPts;


    if (source_is_file)
        moje_obb = new OBB_cpu(input_file);
    else if (source_is_random)
    {
        inPts = getRandomPoints(num_points_generated);
        moje_obb = new OBB_cpu(inPts, num_points_generated);
    }
    else
    {
        inPts = getRandomPoints(100);
        moje_obb = new OBB_cpu(inPts, 100);
    }



    std::vector<glm::vec3> vysledne_body;

    // BRUTE-FORCE METODA CPU
    // -----------------------
    if (run_cpu_bf)
    {
        std::chrono::steady_clock::time_point BF_begin = std::chrono::steady_clock::now();
        OBB_result cpu_bf = moje_obb->calculate_OBB_Bruteforce(16);
        std::chrono::steady_clock::time_point BF_end = std::chrono::steady_clock::now();
        std::cout << "Bruteforce metoda CPU" << std::endl << "==============================" << std::endl << std::flush;
        std::cout << "Cas vypoctu = " << std::chrono::duration_cast<std::chrono::milliseconds>(BF_end - BF_begin).count() << "ms" << std::endl;
        std::cout << "Vysledny objem je " << cpu_bf.volume << std::endl << std::endl;

        if (visualise_cpu_bf)
        {
            vysledne_body.clear();
            vysledne_body.push_back(vec_to_gmlvec3(cpu_bf.vertex));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_bf.x_vec));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_bf.y_vec));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_bf.z_vec));
        }
    }


    // ITERACNI METODA CPU
    // -----------------------
    if (run_cpu_it)
    {
        std::chrono::steady_clock::time_point IT_begin = std::chrono::steady_clock::now();
        OBB_result cpu_it = moje_obb->calculate_OBB_Iteracni();
        std::chrono::steady_clock::time_point IT_end = std::chrono::steady_clock::now();
        std::cout << "Iteracni metoda CPU" << std::endl << "==============================" << std::endl << std::flush;
        std::cout << "Cas vypoctu = " << std::chrono::duration_cast<std::chrono::milliseconds>(IT_end - IT_begin).count() << "ms" << std::endl;
        std::cout << "Vysledny objem je " << cpu_it.volume << std::endl << std::endl;

        if (visualise_cpu_it)
        {
            vysledne_body.clear();
            vysledne_body.push_back(vec_to_gmlvec3(cpu_it.vertex));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_it.x_vec));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_it.y_vec));
            vysledne_body.push_back(vec_to_gmlvec3(cpu_it.z_vec));
        }
    }


    // OPTIMAL METODA Z KNIHOVNY
    // -----------------------
    if (run_cpu_lib)
    {
        std::cout << "Knihovni metoda CPU" << std::endl << "==============================" << std::endl << std::flush;
        std::chrono::steady_clock::time_point OPT_begin = std::chrono::steady_clock::now();
        OBB zknihovny = OBB::OptimalEnclosingOBB(moje_obb->convex_hull);
        std::chrono::steady_clock::time_point OPT_end = std::chrono::steady_clock::now();
        std::cout << "Cas vypoctu = " << std::chrono::duration_cast<std::chrono::milliseconds>(OPT_end - OPT_begin).count() << "ms" << std::endl;
        std::cout << "Vysledny objem je " << zknihovny.Volume() << std::endl << std::endl;
    }


    // BRUTE-FORCE METODA GPU
    // -----------------------

    if (run_gpu_bf)
    {
        std::cout << "Bruteforce metoda GPU 1" << std::endl << "===============================" << std::endl << std::flush;
        std::chrono::steady_clock::time_point GPU_BF_begin = std::chrono::steady_clock::now();
        vector<glm::vec3> gpu_bf = compute_OBB_Bruteforce_GPU(moje_obb->glm_convex_hull,moje_obb->convex_hull.NumVertices(), 16);
        std::chrono::steady_clock::time_point GPU_BF_end = std::chrono::steady_clock::now();
        std::cout << "Cas s rezii = " << std::chrono::duration_cast<std::chrono::milliseconds>(GPU_BF_end - GPU_BF_begin).count() << "ms" << std::endl << std::endl;

        if (visualise_gpu_bf)
        {
            vysledne_body.clear();
            vysledne_body = gpu_bf;
        }
    }


    // BRUTE-FORCE METODA GPU 2
    // -----------------------

    if (run_gpu_bf2)
    {
        std::cout << "Bruteforce metoda GPU 2" << std::endl << "===============================" << std::endl << std::flush;
        std::chrono::steady_clock::time_point GPU_BF_begin2 = std::chrono::steady_clock::now();
        vector<glm::vec3> gpu_bf2 = compute_OBB_Bruteforce_GPU2(moje_obb->glm_convex_hull,moje_obb->convex_hull.NumVertices(), 16);
        std::chrono::steady_clock::time_point GPU_BF_end2 = std::chrono::steady_clock::now();
        std::cout << "Cas s rezii = " << std::chrono::duration_cast<std::chrono::milliseconds>(GPU_BF_end2 - GPU_BF_begin2).count() << "ms" << std::endl;

        if (visualise_gpu_bf2)
        {
            vysledne_body.clear();
            vysledne_body = gpu_bf2;
        }
    }



    // VYKRESLENI
    // -----------------------

    // run visualization
    if (! vysledne_body.empty())
    {
        GMU app(moje_obb->glm_convex_hull, inPts, vysledne_body);
        return app.run();
    }
    else
        return EXIT_SUCCESS;

}



