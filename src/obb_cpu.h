#ifndef OBB_CPU_H
#define OBB_CPU_H

#define PI 3.14159

#include <vector>
#include <string>

#include "MathGeoLib/MathGeoLib.h"
#include "MathGeoLib/Algorithm/Random/LCG.h"
#include "glm/glm.hpp"

glm::vec3 vec_to_gmlvec3(vec v);

struct OBB_result {
    vec vertex;
    vec x_vec, y_vec, z_vec;
    float min_X, min_Y, min_Z;
    float volume;
};
typedef OBB_result OBB_result;

class OBB_cpu
{
public:
    OBB_cpu(std::vector<vec> inPts, int pocet_generovanych_vrcholu);
    OBB_cpu(std::string filename);
    OBB_result calculate_OBB_Bruteforce(int granularita);
    OBB_result calculate_OBB_Iteracni();
    OBB_result calculate_OBB_Optimal();
    OBB_result calculate_AABB_Bruteforce();
    void playground();

    Polyhedron convex_hull;
    std::vector <glm::vec3> glm_convex_hull;

protected:
    OBB_result calculate_OBB_Iteracni_1iterace(OBB_result old, float DELTA_UHEL, int granularita, float old_volume, float EPSILON);
};

#endif // OBB_CPU_H
