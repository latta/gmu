#ifndef OBB_GPU_H
#define OBB_GPU_H

#include <vector>

#include "glm/glm.hpp"

#define PI 3.14159


std::vector<glm::vec3> compute_OBB_Bruteforce_GPU(std::vector<glm::vec3> convex_hull_cpu, int pocet_vrcholu,  int granularita);
std::vector<glm::vec3> compute_OBB_Bruteforce_GPU2(std::vector<glm::vec3> convex_hull_cpu, int pocet_vrcholu,  int granularita);

#endif // OBB_GPU_H
