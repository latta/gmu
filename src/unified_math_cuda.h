    // This implementation follows the code from
    // https://github.com/erwincoumans/experiments/blob/master/opencl/primitives/AdlPrimitives/Math/MathCL.h

     

    #ifndef UNIFIED_MATH_CUDA_H
    #define UNIFIED_MATH_CUDA_H

    /*****************************************
                    Vector
    /*****************************************/

    __device__
        inline float4 getCrossProduct(float4 a, float4 b)
    {
        //float3 v1 = make_float3(a.x, a.y, a.z);
        //float3 v2 = make_float3(b.x, b.y, b.z);
        //float3 v3 = make_float3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
        //
        //return make_float4(v3.x, v3.y, v3.z, 0.0f);

        return make_float4(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x, 0.0f);
    }

    /*****************************************
                    Quaternion
    /*****************************************/

    typedef float4 Quaternion;

    __host__ __device__
        inline Quaternion quaternionMul(Quaternion a, Quaternion b);

    __host__ __device__
        inline float4 qtRotate(Quaternion q, float4 vec);

    __device__
        inline Quaternion qtInvert(Quaternion q);

    /// -------------------------------------------

    __host__ __device__
        inline Quaternion quaternionMul(Quaternion a, Quaternion b)
    {

        /*
        Quaternion ans;
        ans = getCrossProduct(a, b);
        ans = make_float4(ans.x + a.w*b.x + b.w*a.x + b.w*a.y,
                          ans.y + a.w*b.y + b.w*a.z,
                          ans.z + a.w*b.z,
                          ans.w + a.w*b.w + b.w*a.w);
        ans.w = a.w*b.w - (a.x*b.x + a.y*b.y + a.z*b.z);
        //ans.w = a.w*b.w - dot3F4(a, b);
        return ans;
        */


        return make_float4 (
                    a.w*b.x + b.w*a.x + a.y*b.z - a.z*b.y,
                    a.w*b.y + b.w*a.y + a.z*b.x - a.x*b.z,
                    a.w*b.z + b.w*a.z + a.x*b.y - a.y*b.x,
                    a.w*b.w - (a.x*b.x + a.y*b.y + a.z*b.z)
                    );


    }




    __device__
        inline Quaternion qtInvert(const Quaternion q)
    {
        return make_float4(-q.x, -q.y, -q.z, q.w);
    }

    __host__ __device__
        inline float4 qtRotate(const Quaternion q, const float4 vec)
    {
        float4 vcpy = vec;
        vcpy.w = 0.f;
        return quaternionMul(quaternionMul(q,vcpy), make_float4(-q.x, -q.y, -q.z, q.w));
    }

    __device__
        inline float4 qtInvRotate(const Quaternion q, const float4 vec)
    {
        return qtRotate( qtInvert( q ), vec );
    }


    #endif  // UNIFIED_MATH_CUDA_H
