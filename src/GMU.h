#pragma once

#include "BaseApp.h"
#include "glm/glm.hpp"
#include "MathGeoLib/MathGeoLib.h"
#include <SDL2/SDL_image.h>

class GMU: public BaseApp{
public:
    GMU(std::vector<glm::vec3> inPointsHull, std::vector<vec> inPoints, std::vector<glm::vec3> bodyPoints);

    //std::vector<vec> getRandomPoints(int n);

protected:
	/**
	* Init funkce, vola se jednou pri startu aplikace
	*/
	virtual void init();

	/**
	* Draw funkc, vola se v kazdem snimku
	*/
	virtual void draw();

	virtual void onMouseMove(int dx, int dy, int x, int y);
	virtual void onMousePress(Uint8 button, int x, int y);
	virtual void onMouseRelease(Uint8 button, int x, int y);
	virtual void onMouseWheel(int delta);
	virtual void onKeyPress(SDL_Keycode key, Uint16 mod);

	void updateMatrix();

    std::vector<vec> inPoints;
    std::vector<glm::vec3> inPointsHull;
    std::vector<glm::vec3> bodyPoints;

    bool enableHull;
	/* Mys */
	float zoom;
	bool rmbPressed;
	bool lmbPressed;
	int posx, posy;
    int rotx, roty;

	/* Matice */
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;


	/* Opengl objekty */

    GLuint tex;

	/* Buffery */
    GLuint vaoAxes;
    GLuint vboAxes;
    GLuint vaoPoints;
    GLuint vboPoints;
    GLuint vaoPointsHull;
    GLuint vboPointsHull;
    GLuint vaoCube;
    GLuint vboCube;
    GLuint eboCube;

	/* Shadery */
    GLuint vsA, fsA, vsP, fsP, gsP, vsC, fsC, programAxis, programPoints, programCube;

	/* Atributy */
    GLuint positionAttribAxis;
    GLuint positionAttribPoints;
    GLuint positionAttribCube;

	GLuint normalAttrib;

	/* Uniformy */
	GLuint mUniform;
	GLuint vUniform; 
    GLuint pUniform;

    GLuint mUniformP;
    GLuint vUniformP;
    GLuint pUniformP;

    GLuint cubeEnabledUniform;
};
